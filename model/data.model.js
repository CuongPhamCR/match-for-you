var mongoose =  require("mongoose");
var Schema = mongoose.Schema;


var dataSchema  = Schema({
    leagues : String,
    date :  String,
    time : String,
    team1 : String,
    team2 : String
});

var dataModel = mongoose.model("DATA",dataSchema);

module.exports = dataModel;
