var mongoose =  require("mongoose");
var Schema = mongoose.Schema;
var userSchema =  Schema({
    _id : Schema.Types.ObjectId,
    email : String,
    username : String,
    password : String,
    role : [{
        type: Schema.Types.ObjectId,
        ref : 'ROLE'
    }],
    isActive : Number
});


var roleSchema =  Schema({
    _id : Schema.Types.ObjectId,
    ten : String
})

var activeUserSchema = Schema({
    _id : Schema.Types.ObjectId,
    email : [{
        type : String,
        ref : "USER"
    }],
    key : Number
})
var userModel = mongoose.model("USER",userSchema);
var activeUserModel = mongoose.model("ACTIVE_USER",activeUserSchema);
var roleModel = mongoose.model("ROLE",roleSchema);

module.exports = {
    userModel : userModel,
    roleModel : roleModel,
    activeUserModel : activeUserModel
}