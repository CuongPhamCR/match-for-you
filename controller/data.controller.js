var request = require('request');
var cheerio = require('cheerio');
var dataModel = require("../model/data.model");
var mongoose = require("mongoose");

module.exports = {
    deleteData : deleteData,
    showInfoMatch : showInfoMatch,
    calendarMatch : calendarMatch,
    getCalendar : getCalendar
}

function deleteData(){
    const currentDay = new Date();
    return dataModel.find()

    .then(function(data){
        for(var i =0 ; i<data.length; i++){
            var arr = data[i].date.toString().split("/");
            var day = arr[0];
            var month = arr[1];
            var year = arr[2];
            if(year < currentDay.getFullYear()){
                return dataModel.remove({_id : data[i]._id})
                .then((user)=>{
                    if(user.deletedCount > 0){
                        if(i +1 == data.length)
                        return Promise.resolve({
                            message : "Xóa thành công những trận có năm cũ"
                        })
                    }else{
                        return Promise.resolve({
                            message : "Không có trận có năm cũ"
                        })
                    }
                })
                .catch(function(err){
                    return Promise.reject(err);
                })
            }else if(month < currentDay.getMonth() + 1){
                return dataModel.remove({_id : data[i]._id})
                .then((user)=>{
                    if(user.deletedCount > 0){
                        if(i +1  == data.length)
                        return Promise.resolve({
                            message : "Xóa thành công những trận có tháng cũ"
                        })
                    }else{
                        return Promise.resolve({
                            message : "Không có trận có tháng cũ"
                        })
                    }
                })
                .catch(function(err){
                    return Promise.reject(err);
                })
            }else if(day < currentDay.getDate()){
                return dataModel.remove({_id : data[i]._id})
                .then((user)=>{
                    if(user.deletedCount > 0){
                        if(i +1 == data.length)
                        return Promise.resolve({
                            message : "Xóa thành công những trận có ngày cũ"
                        })
                    }else{
                        return Promise.resolve({
                            message : "Không có trận có ngày cũ"
                        })
                    }
                })
                .catch(function(err){
                    return Promise.reject(err);
                })
            }
        }
    })
    .catch(function(err){
        return Promise.reject(err);
    })
}
function getCalendar(){
    return dataModel.find()
        .then((datas) => {
            return Promise.resolve(datas)
        })
        .catch((err) => {
            return Promise.reject(err);
        })    
}
function calendarMatch() {
    request("https://bongdalive.tv/truc-tiep-bong-da/",function(err,respone,body){
        if (err)
        {
            console.log(err);
            res.send("Loi load link");
        }
        else{
            $= cheerio.load(body);
            var trandau1 = $(body).find("div.text-right");
            var trandau2 = $(body).find("div.text-left");
            var date = $(body).find("span.date");
            var time = $(body).find("div.time");
            var leagues = $(body).find("div.leage");
            for(let i=0;i<trandau1.length;i++)
            {
                var data = new dataModel();
                data.leagues = $(leagues[i]).text().trim();
                data.date = $(date[i]).text().trim();
                data.time = $(time[i]).text().trim();
                data.team1 = $(trandau1[i]).text().trim();
                data.team2 = $(trandau2[i]).text().trim();
                data.save();
            }
        }
    });
}
function showInfoMatch(team1,team2,res)
{
    var list = [];
    var j=0;
    list[j]= team1 + "_" + team2;
    j++;
    request("https://ngoac.tv/",function(err,respone,data){
        if(err)
        {
            console.log(err);
        }
        else{
            $ = cheerio.load(data);
            var teamb =$(data).find("a.t_name");
            for(let i=0;i<teamb.length;i++)
            {
                if((team1 == $(teamb[i]).text().trim()) && (team2 == $(teamb[i+1]).text().trim()))
                {
                    list[j]="https://ngoac.tv/";
                    j++;
                    break;
                }
            }
        }
    });
    request("http://keonhacai.net/lich-phat-song-bong-da.html",function(err,respone,data){
        if(err)
        {
            console.log(err);
        }
        else{
            $ = cheerio.load(data);
            var doi1 =$(data).find("h2.team");
            for(let i=0;i<doi1.length-1;i+=2)
            {              
                if((team1 == $(doi1[i]).text().trim()) && (team2 == $(doi1[i+1]).text().trim()))
                {
                    list[j]="http://keonhacai.net/lich-phat-song-bong-da.html";
                    j++;
                    break;
                }
            }
        }
    });
    request("https://bongdalive.tv/truc-tiep-bong-da/",function(err,respone,body){
        if (err)
        {
            console.log(err);
        }
        else{
            $= cheerio.load(body);
            var trandau1 = $(body).find("div.text-right");
            var trandau2 = $(body).find("div.text-left");
            for(let i=0;i<trandau1.length;i++)
            {
                if((team1 == $(trandau1[i]).text().trim()) && (team2 == $(trandau2[i]).text().trim()))
                { 
                    list[j] = "https://bongdalive.tv/truc-tiep-bong-da/";
                    j++;              
                    break;
                }
            }
        }
    });
    request("https://banthang.tv/",function(err,respone,body){
        if (err)
        {
            console.log(err);
        }
        else{
            $= cheerio.load(body);
            var trandau = $(body).find("a.pop-open");    
            for(let i=0;i<trandau.length-2;i++)
            {
                trandau.find("team1");
                if((team1 == $(trandau[i]).text().trim()) && (team2 == $(trandau[i+2]).text().trim()))
                {        
                    list[j] =  "https://banthang.tv/" ;   
                    j++;  
                    break;
                }
            }
        }
    });
    request("https://thevang.tv/truc-tiep.html",function(err,respone,body){
        if (err)
        {
            console.log(err);
        }
        else{
            $= cheerio.load(body);
            var trandau = $(body).find("a.t_name");    
            for(let i=0;i<trandau.length;i+=2)
            {
                if((team1 == $(trandau[i]).text().trim()) && (team2 == $(trandau[i+1]).text().trim()))
                { 
                    list[j] = "https://thevang.tv/truc-tiep.html"; 
                    j++;        
                    break;
                }
            }
        }
    });
    request("https://bongdaf.net/",function(err,respone,body){
        if (err)
        {
            console.log(err);
        }
        else{
            $= cheerio.load(body);
            var trandau1 = $(body).find("div.bf_m_sb_3");  
            var trandau2 = $(body).find("div.bf_m_sb_7");     
            for(let i=0;i<trandau1.length;i++)
            {
                if((team1 == $(trandau1[i]).text().trim()) && (team2 == $(trandau2[i]).text().trim()))
                {    
                    list[j] = "https://bongdaf.net/";
                    j++;    
                    break;
                }

            }
            
        }
    });
    list[j]= "https://suong.me/";
    j++;
    setTimeout(
        () => {
              res.send(list);
            },
            5 * 1000
          );      
}