var nodemailer = require("nodemailer");
var jwt = require("jsonwebtoken");
    var crypto = require('crypto');
var mongoose = require("mongoose");
var modelData = require("../model/user.model")
var cons = require("../cons");


var userModel = modelData.userModel;
var roleModel = modelData.roleModel;
var activeUserModel = modelData.activeUserModel;

var transpoter = nodemailer.createTransport({
    host:"smtp.gmail.com",
    auth:{
        user:"nhutcs123@gmail.com",
        pass:"01214569874a"
    }
})


function quenPass(email){
    var newPass = Math.floor(Math.random() * (999999 - 100000 +1) -100000);
    return transpoter.sendMail({
        from : "Web Da Bong",
        to : email,
        subject : "Cấp lại mật khẩu mới",
        html : "<h1>" + "mật khẩu mới của bạn là " + newPass  + "</h1>"
    })
    .then(()=>{
        return userModel.find({email})
        .then(function(user){
            if(user.length > 0){
                return userModel.update({_id:user[0]._id},{password:newPass}) 
                .then(()=>{
                    return Promise.resolve({
                        message : "reset pass thành công vui lòng kiểm tra email để xem mật khẩu mới"
                    });
                })
                .catch(function(err){
                    return Promise.reject(err);
                })
            }
            else{
                return Promise.reject({
                    statusCode : 400,
                    message : "Email chưa được đăng ký"
                })
            }
        })
        .catch(function(err){
            return Promise.reject(err);
        })
    })
    .catch(function(err){
        return Promise.reject(err);
    })
}

function createRole(){
    roleModel.find().exec(function(err,data){
        if(err){
            res.send(err);
        }else{
            if(data.length === 0){
               var admin = new roleModel({
                   _id : mongoose.Types.ObjectId(),
                   ten : "admin"
               });
               var user =  new roleModel({
                   _id : mongoose.Types.ObjectId(),
                   ten: "user"
               })
               admin.save(function(err){
                   if(err)  res.json({
                       message : err.message
                   })
               })
               user.save(function(err){
                   if(err) res.json({
                    message : err.message
                })
               })
            }
        }
    })   
}

function dangNhap(username,password,key){
    return userModel.find({username})
        .then(function(user){
            if(user.length > 0){
                var hash = crypto.createHmac('sha256', cons.keyPassword)
                .update(password)
                .digest('hex');
                if(hash == user[0].password){
                    var token = jwt.sign({id:user[0]._id, username: username , email:user[0].email, role : user[0].role}, key, { algorithm: "HS256" });
                    return Promise.resolve({
                        message : "Đăng nhập thành công",
                        token : token
                    })
                }
                else{
                    return Promise.reject({
                        statusCode : 404,
                        message : "Sai tên đăng nhập hoặc mật khẩu"
                    })
                }
            }else{
                return Promise.reject({
                    statusCode : 404,
                    message : "Sai tên đăng nhập hoặc mật khẩu"
                })
            }
        })
        .catch(function(err){
            return Promise.reject(err);
        })
}
function sendMail(email,key){
    return transpoter.sendMail({
        from:"Web Đá Bóng",
        to: email,
        subject:"Xác nhận đăng ký thành viên web đá bóng",
        html:"<h1>Đây là mã xác thực tài khoản của bạn: " + key +"</h1>"
    }).then(()=>{
        return Promise.resolve({
            message : "Gửi mail thành công"
        })
    })
    .catch(function(err){
        return Promise.reject(err);
    })
}
function createActiveUser(email,keyActive){
    var active = new activeUserModel({
        _id : mongoose.Types.ObjectId(),
        email : email,
        key : keyActive
    });
    return active.save()
    .then(function(ac){
        return Promise.resolve(ac);
    })
    .catch(function(err){
        return Promise.reject(err);
    })
}
function dangKy(keyActive,newUser){
    return sendMail(newUser.email,keyActive)
    .then(()=>{
        return createActiveUser(newUser.email,keyActive)
        .then(function(ac){
            return Promise.resolve(ac);
        })
        .catch(function(err){
            return Promise.reject(err);
        })
    })
    .catch(function(err){
        return Promise.reject(err);
    })
}

function xacThuc(email,key){
    return activeUserModel.find({email})
        .then(function(activeUser){
            if(activeUser.length > 0){
                if(activeUser[0].key == key){
                    return userModel.find({email})
                    .then(function(user){
                       return userModel.update({_id:user[0]._id},{isActive:1})
                            .then(function(userUpdate){
                                return activeUserModel.remove({email})
                                .then(()=>{
                                    return Promise.resolve({
                                        message : "Tài khoản của bạn đã được xác thực thành công"
                                    });
                                })
                                .catch(function(err){
                                    return Promise.reject(err);
                                })
                                .catch(function(err){
                                    return Promise.reject(err);
                                })
                            })
                            .catch(function(err){
                                return Promise.reject(err);
                            })
                    })
                    .catch(function(err){
                        return Promise.reject(err);
                    })
                }else{
                    return Promise.reject({
                        statusCode : 400,
                        message : "key không đúng"
                    })
                }
            }else{
                return Promise.reject({
                    statusCode : 400,
                    message : "user chưa được đăng ký"
                })
            }
        })
}


function getUser(amount,page,search,typesort){
    var total = 0;
    userModel.find().count(function(err,data){
        total = data;
    });
   return userModel.find({username: {$regex: search,$options:"$i"}}).limit(amount)
   .skip(amount*page).sort({username : typesort})
   .populate("role")
    .then(function(user){
        user.push({
            total:  total,
            page : page,
            amount:amount,
            typesort : typesort,
            search : search
        });
        return Promise.resolve(user);
    })
    .catch(function(err){
        return Promise.reject(err);
    })

}
function getUserById(id){
    return userModel.find({_id:id})
    .then(function(user){
        return Promise.resolve(user);
    })
    .catch(function(err){
        return Promise.reject({
            statusCode : 404,
            message : "id Không đúng"
        });
    })
}
function checkEmail(email){
    return userModel.find({email:email})
    .then(function(user){
        return Promise.resolve(user);
    })
    .catch(function(err){
        return Promise.reject(err);
    })
}
function createUser(username,password,email,role,isActive){
    return userModel.find({email : email})
        .then(function(user){
            if(user.length > 0 ){
                return Promise.reject({
                    statusCode : 400,
                    message : "user đã tồn tại"
                })
            }else{
               return userModel.find({username : username})
               .then(function(data){
                if(data.length > 0){
                    return Promise.reject({
                        statusCode : 400,
                        message : "user đã tồn tại"
                    })
                }else{
                    var hash = crypto.createHmac('sha256', cons.keyPassword)
                    .update(password)
                    .digest('hex');
                    password = hash;
                    return roleModel.find({ten:role})
                        .then(function(role){
                            var user = new userModel({
                                _id : mongoose.Types.ObjectId(),
                                username : username,
                                password : password,
                                email : email,
                                isActive : isActive,
                                role : role[0]._id
                            });
                            return user.save()
                                .then(function(user){
                                    return Promise.resolve(user);
                                })
                                .catch(function(err){
                                    return Promise.reject(err);
                                })
                        })
                        .catch(function(err){
                            return Promise.reject(err);
                        })
                }
               })
               .catch(function(err){
                   return Promise.reject(err);
               })
            }
        })
        .catch(function(err){
            return Promise.reject(err);
        })
}

function updateUser(_id,username, password, email,role,isActive){
   return userModel.update({_id : _id}, {username : username,password : password,email : email,role : role,isActive : isActive})
   .then(function(user){
        return Promise.resolve(user);
   })
   .catch(function(err){
       return Promise.reject(err);
   })
}

function deleteUser(id){
    return userModel.remove({_id : id})
    .then(function(user){
        return Promise.resolve(user);
   })
   .catch(function(err){
       return Promise.reject(err);
   })
}
function getIdRole(role){
    return roleModel.find({ten : role})
        .then(function(role){
            return Promise.resolve(role);
        })
        .catch(function(err){
            return Promise.reject(err);
        })
}

function createAdmin(){
    userModel.find()
    .then(function(user){
        if(user.length == 0){
            createUser("admin","admin","nhutcs123@gmail.com","admin",1)
            .then(function(data){
                console.log(data);
            })
            .catch(function(err){
                console.log(err);
            })
        }
    })
    .catch(function(err){
        console.log(err)
    })
    
}
module.exports = {
    dangNhap : dangNhap,
    dangKy : dangKy,
    xacThuc : xacThuc,
    createRole : createRole,
    quenPass: quenPass,
    getUser : getUser,
    createUser : createUser,
    updateUser : updateUser,
    deleteUser : deleteUser,
    getIdRole : getIdRole,
    checkEmail : checkEmail,
    createAdmin : createAdmin,
    getUserById : getUserById,
    sendMail : sendMail,
    createActiveUser : createActiveUser
}
