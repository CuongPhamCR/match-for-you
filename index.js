var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var userRouter = require("./routes/user.routes");
var adminRouter = require("./routes/admin.routes");
var userController = require("./controller/user.controller");
var dataRouter = require('./routes/data.router');
var cronData = require("./crondata");
var app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use("/api",userRouter);
app.use("/admin",adminRouter);
app.use('/api/data',dataRouter);

var connect = mongoose.connect("mongodb://localhost:27017/WebDaBong");

userController.createRole();
userController.createAdmin();

cronData.jobGetData.start();
cronData.jobDelete.start();

app.listen(3000, function () {
    console.log("ung dung chay tren port 3000");
});





