var router = require("express").Router();
var jwt = require("jsonwebtoken");
var xacThucToken = require("../middleware/checkToken")
var userController = require("../controller/user.controller");
var cons = require("../cons");
var crypto = require('crypto');

router.post("/",xacThucToken.xacThucAdmin,createUser);
router.get("/",xacThucToken.xacThucAdmin,getUser);
router.put("/",xacThucToken.xacThucAdmin,updateUser);
router.delete("/",xacThucToken.xacThucAdmin,deleteUser);


module.exports = router;



function getUser(req,res){
    var page =0;
    var amount= 10;
    var typesort = -1;
    var search = "";
    if(req.body.amount !== undefined)
        amount = parseInt(req.body.amount);
    if(req.body.page !== undefined)
        page = parseInt(req.body.page);
    if(req.body.search !== undefined)
        search = req.body.search;
    if(req.body.typesort !== undefined)
        typesort = parseInt(req.body.typesort);
    userController.getUser(amount,page,search,typesort)
    .then(function(data){
        res.json(data);
    })
    .catch(function(err){
        res.json(err);
    })
}

function createUser(req,res){
    var username = req.body.username;
    if(username == undefined){
        res.json({
            statusCode : 400,
            message : "Bạn chưa nhập username"
        })
        return;
    }
    if(username.length < 1 || username.length > 15){
        res.json({
            statusCode : 400,
            message : "username phải từ 1 đến 15 ký tự"
        })
        return;
    }
        
    var password = req.body.password;
    if(password == undefined){
        res.json({
            statusCode : 400,
            message : "Bạn chưa nhập password"
        })
        return;
    }
    if(password.length < 5 || password.length > 15){
        res.json({
            statusCode : 400,
            message : "password phải từ 5 đến 15 ký tự"
        })
        return;
    }
    var email = req.body.email;
    if(email == undefined){
        res.json({
            statusCode : 400,
            message : "Bạn chưa nhập email"
        })
        return;
    }
    if(!cons.paterEmail.test(email)){
        res.json({
            statusCode : 400,
            message : "email không đúng định dạng (ex: abc@gmail.com)"
        })
        return;
    }
    var role = req.body.role;
    if(role == undefined){
        res.json({
            statusCode : 400,
            message : "Bạn chưa nhập role"
        })
    }
     userController.createUser(username,password,email,role,1)
     .then(()=>{
         res.json({
             message : "Thêm user thành công"
         })
     })
     .catch(function(err){
         res.json(err);
     })
}

function updateUser(req,res){
    var username = req.body.username;
    var password = req.body.password;
    var email = req.body.email;
    var role = req.body.role;
    var isActive = req.body.active;
    var id = req.body.id;
    if(id == undefined){
        res.json({
            statusCode : 400,
            message : "Bạn chưa nhập id"
        });
        return;
    }
    userController.getUserById(id)
    .then(function(user){
        if(user.length > 0){
            if(username == undefined && password == undefined && isActive == undefined
                && role == undefined && email == undefined){
                    res.json({
                        message : "Không có thông tin mới cần được cập nhập"
                    })
                    return;
                }
            if(username == undefined){
                username = user[0].username;
            }
            if(username.length < 1 || username.length > 15){
                res.json({
                    statusCode : 400,
                    message : "username phải từ 1 đến 15 ký tự"
                })
                return;
            }
            if(isActive == undefined){
                isActive = user[0].isActive;
            }
            var isChange =true;
            if(password == undefined){
                password = user[0].password;
                isChange = false;
            }
            if(isChange && (password.length < 5 || password.length > 15)){
                res.json({
                    statusCode : 400,
                    message : "password phải từ 5 đến 15 ký tự"
                })
            }
            if(email == undefined){
                email = user[0].email;
            }
            if(!cons.paterEmail.test(email)){
                res.json({
                    statusCode : 400,
                    message : "email không đúng định dạng (ex: abc@gmail.com)"
                })
                return;
            }
            var isChangRole = true;
            if(role == undefined){
                role = user[0].role;
                isChangRole = false;
            }
            if(isChangRole){
                userController.getIdRole(role.toLowerCase())
                .then(function(foundRole){
                    var hash = crypto.createHmac('sha256', cons.keyPassword)
                    .update(password)
                    .digest('hex');
                    password = hash;
                userController.updateUser(id,username,password,email,foundRole[0]._id,isActive)
                .then(()=>{
                    res.json({
                        message : "Update user thành công"
                    })
                })
                .catch(function(err){
                    res.json(err);
                })
            })
            .catch(function(err){
                res.json(err);
            })
            }else{
                var hash = crypto.createHmac('sha256', cons.keyPassword)
                .update(password)
                .digest('hex');
                password = hash;
                userController.updateUser(id,username,password,email,role[0]._id,isActive)
                .then(()=>{
                    res.json({
                        message : "Update user thành công"
                    })
                })
                .catch(function(err){
                    res.json(err);
                })
            }
        }else{
            res.json({
                message : "Không tìm thấy user"
            })
        }
    })
    .catch(function(err){
        res.json(err);
    })
    
}

function deleteUser(req,res){
    var id = req.body.id;
    if(id == undefined){
        res.json({
            statusCode : 400,
            message : "Bạn chưa nhập id"
        })
        return;
    }
    userController.deleteUser(id)
    .then((user)=>{
        if(user.deletedCount > 0){
            res.json({
                message : "Delete user thành công"
            })
        }else{
            res.json({
                message : "không tìm thấy user"
            })
        }
        
    })
    .catch(function(err){
        res.json(err);
    })
}