var router = require("express").Router();
var jwt = require("jsonwebtoken");
var xacThucToken = require("../middleware/checkToken")
var userController = require("../controller/user.controller");
var cons = require("../cons");
var crypto = require('crypto');

router.post("/resetpassword",resetPassword);
router.post("/dangnhap",dangNhap);
router.post("/dangky",dangKy);
router.post("/xacthuc",xacThucToken.xacThucUser,xacThuc);
router.put("/update",xacThucToken.xacThucUser,updateUser)
router.get("/get",xacThucToken.xacThucUser,getUser)


module.exports = router;

function getUser(req,res){
    var token = req.headers.token;
    jwt.verify(token, cons.keyToken, function (err, decode) {
        userController.getUserById(decode.id)
        .then(function(user){
            res.json(user);
        })
        .catch(function(err){
            res.json(err);
        })
    })
}
function updateUser(req,res){
    var username = req.body.username;
    var password = req.body.password;
    var email = req.body.email;
    var token = req.headers.token;
    jwt.verify(token, cons.keyToken, function (err, decode) {
        userController.getUserById(decode.id)
        .then(function(user){
            if(user.length > 0){
                if(username == undefined && password == undefined && email == undefined){
                        res.json({
                            message : "Không có thông tin mới cần được cập nhập"
                        })
                        return;
                    }
                if(username == undefined){
                    username = user[0].username;
                }
                if(username.length < 1 || username.length > 15){
                    res.json({
                        statusCode : 400,
                        message : "username phải từ 1 đến 15 ký tự"
                    })
                    return;
                }
                var isChangePass = true;
                if(password == undefined){
                    password = user[0].password;
                    isChangePass = false;
                }
                if(isChangePass && (password.length < 5 || password.length > 15)){
                    res.json({
                        statusCode : 400,
                        message : "Password phải từ 5 đến 15 ký tự"
                    })
                    return;
                }
                var isChange = true;
                if(email == undefined){
                    email = user[0].email;
                    isChange = false;
                }
                if(!cons.paterEmail.test(email)){
                    res.json({
                        statusCode : 400,
                        message : "Email không đúng định dạng (ex: abc@gmail.com)"
                    })
                    return;
                }
                if(isChange){
                    var keyActive = Math.floor(Math.random()*(9999 - 1000 +1) + 1000);
                    userController.checkEmail(email)
                    .then(function(user){
                        if(user.length > 0){
                            res.json({
                                statusCode : 400,
                                message : "Email đã được đăng ký"
                            })
                        }else{
                            userController.sendMail(email,keyActive)
                    .then(()=>{
                        userController.createActiveUser(email,keyActive)
                        .then(()=>{
                            userController.getIdRole("user")
                            .then(function(foundRole){
                            var token = req.headers.token;
                            jwt.verify(token, cons.keyToken, function (err, decode) {
                                var hash = crypto.createHmac('sha256', cons.keyPassword)
                                .update(password)
                                .digest('hex');
                                password = hash;
                                userController.updateUser(decode.id,username,password,email,foundRole[0]._id,0)
                                .then(()=>{
                                    res.json({
                                        message : "Update user thành công, Kiểm tra email để kích hoạt lại tài khoản"
                                    })
                                })
                                .catch(function(err){
                                    res.json(err);
                                })
                            })
                        })
                        .catch(function(err){
                            res.json(err);
                        })
                        })
                        .catch(function(err){
                            res.json(err);
                        })
                       
                    })
                        }
                    })
                    .catch(function(err){
                        res.json(err);
                    })
                    .catch(function(err){
                        res.json(err);
                    })
                }else{
                    userController.getIdRole("user")
                    .then(function(foundRole){
                    var token = req.headers.token;
                    jwt.verify(token, cons.keyToken, function (err, decode) {
                        userController.getUserById(decode.id)
                        .then(function(user){
                            var hash = crypto.createHmac('sha256', cons.keyPassword)
                            .update(password)
                            .digest('hex');
                            password = hash;
                            userController.updateUser(decode.id,username,password,email,foundRole[0]._id,user[0].isActive)
                            .then(()=>{
                                res.json({
                                    message : "Update user thành công"
                                })
                            })
                            .catch(function(err){
                                res.json(err);
                            })
                        })
                        .catch(function(err){
                            res.json(err);
                        })
                    })
                })
                .catch(function(err){
                    res.json(err);
                })
                }
            }else{
                res.json({
                    statusCode : 400,
                    message : "Không tìm thấy user"
                })
            }
        })
        .catch(function(err){
            res.json(err);
        })
    })
}

function resetPassword(req,res){
    var email = req.body.email;
    if(email == undefined){
        res.json({
            statusCode : 400,
            message : "Bạn chưa nhập email"
        })
        return;
    }
    if(!cons.paterEmail.test(email)){
        res.json({
            statusCode : 400,
            message : "Email không đúng định dạng (ex: abc@gmail.com)"
        })
        return;
    } 
    userController.quenPass(email)
    .then((data)=>{
        res.json(data);
    })
    .catch(function(err){
        res.json(err);
    })
}

function dangNhap (req, res) {
    var username = req.body.username;
    if(username == undefined ){
        res.json({
            statusCode : 400,
            message : "Bạn chưa nhập username"
        })
        return;
    }
    if(username.length < 1 || username.length > 15){
        res.json({
            statusCode : 400,
            message : "username phải từ 1 đến 15 ký tự"
        })
        return;
    }
    var password = req.body.password;
    if(password == undefined ){
        res.json({
            statusCode : 400,
            message : "Bạn chưa nhập password"
        })
        return;
    }
    if(password.length < 5 || password.length > 15){
        res.json({
            statusCode : 400,
            message : "password phải từ 5 đến 15 ký tự"
        })
        return;
    }
    userController.dangNhap(username,password,cons.keyToken)
    .then(function(data){
        res.json(data);
    })
    .catch(function(err){
        res.json(err);
    })
}

function dangKy (req, res) {
    var username = req.body.username;
    if(username == undefined){
        res.json({
            statusCode : 400,
            message : "Bạn chưa nhập username"
        })
        return;
    }
    if(username.length < 1 || username.length > 15){
        res.json({
            statusCode : 400,
            message : "username phải từ 1 đến 15 ký tự"
        })
        return;
    }
    var password = req.body.password;
    if(password == undefined){
        res.json({
            statusCode : 400,
            message : "Bạn chưa nhập password"
        })
        return;
    }
    if(password.length < 5 || password.length > 15){
        res.json({
            statusCode : 400,
            message : "password phải từ 5 đến 15 ký tự"
        })
        return;
    }
    var email = req.body.email;
    if(email == undefined){
        res.json({
            statusCode : 400,
            message : "Bạn chưa nhập email"
        })
        return;
    }
    if(!cons.paterEmail.test(email)){
        res.json({
            statusCode : 400,
            message : "email không đúng định dạng (ex: abc@gmail.com)"
        })
        return;
    }
    var keyActive = Math.floor(Math.random()*(9999 - 1000 +1) + 1000);
    userController.createUser(username,password,email,'user',0)
    .then(function(oldUser){
        userController.dangKy(keyActive,oldUser)
        .then(function(ac){
            res.json({
                message : "vui lòng kiểm tra mail để hoàn tất quá trình đăng ký"
            })
        })
        .catch(function(err){
            res.send(err);
        })
    })
    .catch(function(err){
        res.json(err);
    })
    
  
}
function xacThuc(req,res){
    var key = req.body.key;
    if( key == undefined){
        res.json({
            statusCode : 400,
            message : "Bạn chưa nhập key"
        })
        return;
    }
    var token = req.headers.token;
    jwt.verify(token, cons.keyToken, function (err, decode) {
        userController.xacThuc(decode.email,key)
        .then(function(data){
            res.json(data);
        })
        .catch(function(err){
            res.json(err);
        })
    })
     
}