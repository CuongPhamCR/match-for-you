var router = require('express').Router();
var dataController = require('../controller/data.controller');

router.get('/showinfo',showInfo);
router.get('/calendar',getCalender);

module.exports = router;

function showInfo(req,res)
{
    var team1 = req.query.team1;
    var team2 = req.query.team2;
    dataController.showInfoMatch(team1,team2,res);
}
function getCalender(req,res,next)
{
    dataController.getCalendar()
        .then((datas)=>{
            return res.json({
                datas: datas
            })
        })
        .catch((err)=>{
            return next(err);
        })
}