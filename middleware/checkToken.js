var jwt = require("jsonwebtoken");
var userController = require("../controller/user.controller");
var cons = require("../cons");
function xacThucUser (req, res, next) {
    if (req.headers.token) {
        var token = req.headers.token;
        jwt.verify(token, cons.keyToken, function (err, decode) {
            if (err) {
                res.json({
                    statusCode : 404,
                    message : "Token không chính xác"
                });
            }
            else {
                next();
            }
        })
    }
    else {
        res.json({
            statusCode : 404,
            message : "Bạn chưa nhập token"
        });
    }
}

function xacThucAdmin (req, res, next) {
    if (req.headers.token) {
        var token = req.headers.token;
        jwt.verify(token, cons.keyToken, function (err, decode) {
            if (err) {
                res.json({
                    statusCode : 404,
                    message : "Token không chính xác"
                });
            }
            else {
                userController.getIdRole("admin")
                .then(function(role){
                    if(role[0]._id == decode.role[0]){
                        next();
                    } 
                    else{
                        res.json({
                            statusCode : 400,
                            message : "Bạn không phải là Admin"
                        })
                    }
                })
                .catch(function(err){
                    res.json(err);
                })
               
            }
        })
    }
    else {
        res.json({
            statusCode : 404,
            message : "Bạn chưa đăng nhập"
        });
    }
}

module.exports ={
    xacThucUser : xacThucUser,
    xacThucAdmin : xacThucAdmin
} 
