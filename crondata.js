var cronJob = require('cron').CronJob;
var dataController = require('./controller/data.controller');

var jobGetData = new cronJob({
    cronTime :  '00 00 00 * * 0-6', // chạy vào 00: 00 hang dem
    onTick : function(){
        dataController.calendarMatch();
        console.log('Cron jub runing...');
    },
    start : true,
    timeZone : 'Asia/Ho_Chi_Minh'
});
var jobDelete = new cronJob({
    cronTime :   "00 00 00 * * 1,4", // chạy vào thứ 2 và thứ 5 hằng tuần
    //cronTime : "*/10 * * * * *", //test
    onTick : function(){
        dataController.deleteData()
        .then(function(ketQua){
            console.log(ketQua);
        })
        .catch(function(err){
            console.log(err);
        })
    },
    start : true,
    timeZone : 'Asia/Ho_Chi_Minh'
});
module.exports = {
    jobGetData : jobGetData,
    jobDelete : jobDelete
}
